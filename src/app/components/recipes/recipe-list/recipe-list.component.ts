import { Component, OnInit } from '@angular/core';
import { Recipe } from '../recipe.model';

@Component({
  selector: 'recipe-list-component',
  templateUrl: './recipe-list.component.html'
})
export class RecipeListComponent implements OnInit {
  recipes: Recipe[] = [
    new Recipe('Chicha Morada', 'this is a description', 'https://loremflickr.com/200/300/chicha'),
    new Recipe('Canapés de Malta', 'this is a description', 'https://loremflickr.com/200/300/canape'),
    new Recipe('Pisco Sour', 'This is a pisco sour description', 'https://loremflickr.com/200/300/pisco')
  ];
  constructor() { }
  ngOnInit() { }
}
